var app = angular.module('myApp');

app.factory('LoginService', function ($http) {
    var admin = 'admin';
    var pass = 'password';
    var isAuthenticated = false;
    //var credentials = { 'admin': username, 'password': password};

    return {
        login: function (username, password) {
            // isAuthenticated = username === admin && password === pass;
            $http({
                url: 'http://localhost:8080/rest/login',
                method: "POST",
                data: {'email': username, 'password': password}
            }).then(function (response) {
                    alert(response.data);
                    isAuthenticated = true;
                },
                function (response) { // optional
                    isAuthenticated = false;
                });

            //return isAuthenticated;
        },
        isAuthenticated: function () {
            return isAuthenticated;
        }
    };

});