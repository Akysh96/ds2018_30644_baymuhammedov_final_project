/**
 * Created by akysh_000 on 12/16/2018.
 */
var app = angular.module('myApp');
app.controller('HomeController',
    function ($http, $scope, $rootScope, $stateParams, $state, $window) {
        $scope.user = $window.localStorage.getItem("username");
        $http.get('http://localhost:8080/rest/request/all').then(function (response) {
            $scope.listRequests = response.data;
        });

        $http.get('http://localhost:8080/rest/reason/all').then(function (response) {
            $scope.listReasons = response.data;
        });

        $scope.acceptRequest = function (id) {
            $http({
                url: 'http://localhost:8080/rest/request?id=' + id,
                method: "PUT",
                data: {'status': 'accepted'}
            }).then(function (response) {
                $window.location.reload();
            });
        };

        $scope.rejectRequest = function (id) {
            $http({
                url: 'http://localhost:8080/rest/request?id=' + id,
                method: "PUT",
                data: {'status': 'rejected'}
            }).then(function (response) {
                $window.location.reload();
            });
        };

        $scope.openTab = function (evt, tabName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(tabName).style.display = "block";
            evt.currentTarget.className += " active";
        };

        $scope.readMessage = function (evt, email, motive) {
            $scope.email = email;
            $scope.motive = motive;
            $scope.openTab(evt, "ViewMessage");
        }
    });