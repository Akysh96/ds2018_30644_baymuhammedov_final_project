/**
 * Created by akysh_000 on 12/16/2018.
 */
var app = angular.module('myApp');
app.controller('LoginController', function ($http, $scope, $rootScope, $stateParams, $state, $window) {
    $rootScope.title = "AngularJS Login Sample";

    $scope.formSubmit = function () {
        $http({
            url: 'http://localhost:8080/rest/login',
            method: "POST",
            data: {'email': $scope.username, 'password': $scope.password}
        }).then(function (response) {
                $window.localStorage.setItem("username", $scope.username);
                $rootScope.userName = $scope.username;
                $scope.error = '';
                $scope.username = '';
                $scope.password = '';
                if (response.data == 1) {
                    $state.transitionTo('home');
                }
                if (response.data == 0) {
                    $state.transitionTo('employee');
                }
                else {
                    $scope.error = "Incorrect username/password !";
                }
            },
            function (response) { // optional
                $scope.error = "Incorrect username/password !";
            });
    };
});