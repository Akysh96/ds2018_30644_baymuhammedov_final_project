/**
 * Created by akysh_000 on 12/16/2018.
 */
var app = angular.module('myApp');
app.controller('EmployeeController',
    function ($http, $scope, $rootScope, $stateParams, $state, $window, $dialog) {
        $scope.user = $window.localStorage.getItem("username");

        $http.get('http://localhost:8080/rest/request/all').then(function (response) {
            $scope.listRequests = response.data;
        });

        $http.get('http://localhost:8080/rest/reason/all').then(function (response) {
            $scope.listReasons = response.data;
        });

        $http.get('http://localhost:8080/rest/missedhours?email=' + $scope.user).then(function (response) {
            $scope.missedhours = response.data;
        });

        $scope.openTab = function (evt, tabName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(tabName).style.display = "block";
            //evt.currentTarget.className += " active";
        };

        $scope.readMessage = function (evt, email, motive) {
            $scope.email = email;
            $scope.motive = motive;
            $scope.openTab(evt, "ViewMessage");
        };

        var dialogOptions = {
            controller: 'EditCtrl',
            templateUrl: 'requestPopup.html'
        };

        $scope.request = function () {

            $dialog.dialog(angular.extend(dialogOptions))
                .open()
        };

        $scope.reason = function () {
            $state.transitionTo('request');
        }
    }
);

function EditCtrl($scope, $rootScope, $http, $window, dialog) {

    $scope.user = $window.localStorage.getItem("username");
    $scope.types = ["vacation", "promotion"];

    $scope.save = function () {
        $http({
            url: 'http://localhost:8080/rest/request',
            method: "POST",
            data: {'email': $scope.user, 'type': $scope.type, 'status': 'not responded'}
        }).then(function (response) {
                $window.location.reload();
            },
            function (response) { // optional
                alert("error!!");
            });
        dialog.close();
    };

    $scope.close = function () {
        dialog.close();
    };
}