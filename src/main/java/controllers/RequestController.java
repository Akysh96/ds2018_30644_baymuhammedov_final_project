package controllers;

import bll.RequestBll;
import entity.BaseResponse;
import entity.Request;
import entity.Response;
import exceptions.NotFoundException;
import org.springframework.web.bind.annotation.*;
import services.MailService;

import java.util.List;

/**
 * Created by akysh_000 on 12/8/2018.
 */
@RestController
@RequestMapping("/request")
public class RequestController {

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Request> getAll() {
        return RequestBll.getAllRequests();
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public Request get(@RequestParam(value = "id") int id) {
        Request request = RequestBll.getRequest(id);
        if (request != null) {
            return request;
        }
        throw new NotFoundException();
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public BaseResponse post(@RequestBody Request request) {
        BaseResponse response = new BaseResponse();
        if (request.getEmail() != null) {
            int id = RequestBll.insertRequest(request);
            if (id > 0) {
                Thread thread = new Thread(() -> MailService.getInstance().sendMail("akanyabaymuhammedov@gmail.com", "Request", "Request type: " + request.getType() + " from: " + request.getEmail()));
                thread.start();
                response.setCode(Response.CODE_SUCCESS);
                response.setStatus(Response.SUCCESS_STATUS);
                return response;
            }
        }
        response.setCode(Response.AUTH_FAILURE);
        response.setStatus(Response.ERROR_STATUS);
        return response;
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public BaseResponse put(@RequestParam(value = "id") int id, @RequestBody Request request) {
        BaseResponse response = new BaseResponse();
        Request request1 = RequestBll.getRequest(id);
        if (request1 == null || request == null) {
            response.setStatus(Response.ERROR_STATUS);
            response.setCode(Response.AUTH_FAILURE);
            return response;
        }
        request1.setStatus(request.getStatus());
        RequestBll.updateRequest(request1);
        Thread thread = new Thread(() -> MailService.getInstance().sendMail(request1.getEmail(), "Response for your request", "Your request for: " + request1.getType() + " Has been: " + request1.getStatus()));
        thread.start();
        response.setStatus(Response.SUCCESS_STATUS);
        response.setCode(Response.CODE_SUCCESS);
        return response;
    }


    @RequestMapping(value = "", method = RequestMethod.DELETE)
    public BaseResponse delete(@RequestParam(value = "id") int id) {
        BaseResponse response = new BaseResponse();
        int result = RequestBll.deleteRequest(id);
        if (result < 0) {
            response.setStatus(Response.ERROR_STATUS);
            response.setCode(Response.AUTH_FAILURE);
            return response;
        }
        response.setStatus(Response.SUCCESS_STATUS);
        response.setCode(Response.CODE_SUCCESS);
        return response;
    }
}
