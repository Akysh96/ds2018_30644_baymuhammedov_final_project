package controllers;

import bll.MissedHoursBll;
import entity.BaseResponse;
import entity.MissedHours;
import entity.Response;
import exceptions.NotFoundException;
import org.springframework.web.bind.annotation.*;

/**
 * Created by akysh_000 on 1/12/2019.
 */
@RestController
@RequestMapping("/missedhours")
public class MissedHoursController {

    @RequestMapping(value = "", method = RequestMethod.GET)
    public int get(@RequestParam(value = "email") String email) {
        int missedHours = MissedHoursBll.getMissedHours(email);
        if (missedHours > -1) {
            return missedHours;
        }
        throw new NotFoundException();
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public BaseResponse post(@RequestBody MissedHours missedHours) {
        BaseResponse response = new BaseResponse();
        if (missedHours != null) {
            MissedHoursBll.insertMissedHours(missedHours);
            response.setCode(Response.CODE_SUCCESS);
            response.setStatus(Response.SUCCESS_STATUS);
            return response;

        }
        response.setCode(Response.AUTH_FAILURE);
        response.setStatus(Response.ERROR_STATUS);
        return response;
    }

}