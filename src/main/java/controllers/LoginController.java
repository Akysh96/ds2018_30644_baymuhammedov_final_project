package controllers;

import bll.CredentialsBll;
import bll.EmployeeBll;
import entity.BaseResponse;
import entity.Credentials;
import entity.Employee;
import entity.Response;
import exceptions.NotFoundException;
import org.springframework.web.bind.annotation.*;

/**
 * Created by akysh_000 on 12/8/2018.
 */

@RestController
@RequestMapping("/login")
public class LoginController {

    @RequestMapping(value = "", method = RequestMethod.POST)
    public int post(@RequestBody Credentials credentials) {
        System.out.println("login!!!");
        System.out.println("email: " + credentials.getEmail() + " password: " + credentials.getPassword());
        return CredentialsBll.readType(credentials);
    }
}
