package controllers;

import bll.EmployeeBll;
import entity.BaseResponse;
import entity.Employee;
import entity.Response;
import exceptions.NotFoundException;
import org.springframework.web.bind.annotation.*;

/**
 * Created by akysh_000 on 12/8/2018.
 */

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @RequestMapping(value = "", method = RequestMethod.GET)
    public Employee get(@RequestParam(value = "email") String email) {
        Employee employee = EmployeeBll.getEmployee(email);
        if (employee != null) {
            return employee;
        }
        throw new NotFoundException();
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public BaseResponse post(@RequestBody Employee employee) {
        BaseResponse response = new BaseResponse();
        if (employee != null) {
            System.out.println("email: " + employee.getEmail());
            int id = EmployeeBll.insertEmployee(employee);
            System.out.println("id: " + id);
            if (id == 0) {
                response.setCode(Response.AUTH_FAILURE);
                response.setStatus(Response.ERROR_STATUS);
            } else {
                response.setCode(Response.CODE_SUCCESS);
                response.setStatus(Response.SUCCESS_STATUS);
            }
        } else {
            response.setCode(Response.AUTH_FAILURE);
            response.setStatus(Response.ERROR_STATUS);
        }
        return response;
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public BaseResponse put(@RequestParam(value = "id") int id, @RequestBody Employee employee) {
        BaseResponse response = new BaseResponse();
        Employee employee1 = EmployeeBll.getEmployeeById(id);
        if (employee1 == null || employee == null) {
            response.setStatus(Response.ERROR_STATUS);
            response.setCode(Response.AUTH_FAILURE);
            return response;
        }
        employee.setId(id);
        EmployeeBll.updateEmployee(employee);
        response.setStatus(Response.SUCCESS_STATUS);
        response.setCode(Response.CODE_SUCCESS);
        return response;
    }


    @RequestMapping(value = "", method = RequestMethod.DELETE)
    public BaseResponse delete(@RequestParam(value = "id") int id) {
        BaseResponse response = new BaseResponse();
        int result = EmployeeBll.deleteEmployee(id);
        if (result < 0) {
            response.setStatus(Response.ERROR_STATUS);
            response.setCode(Response.AUTH_FAILURE);
            return response;
        }
        response.setStatus(Response.SUCCESS_STATUS);
        response.setCode(Response.CODE_SUCCESS);
        return response;
    }
}
