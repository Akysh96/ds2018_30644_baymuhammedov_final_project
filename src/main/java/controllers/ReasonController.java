package controllers;

import bll.EmployeeBll;
import bll.MissedHoursBll;
import bll.ReasonBll;
import entity.*;
import exceptions.NotFoundException;
import org.springframework.web.bind.annotation.*;
import services.MailService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by akysh_000 on 12/8/2018.
 */
@RestController
@RequestMapping("/reason")
public class ReasonController {

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Reason> getAll() {
        return ReasonBll.getAllReasons();
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public Reason get(@RequestParam(value = "id") int id) {
        Reason reason = ReasonBll.getReason(id);
        if (reason != null) {
            return reason;
        }
        throw new NotFoundException();
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public BaseResponse post(@RequestBody Reason reason) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date date = new Date();
        BaseResponse response = new BaseResponse();
        if (reason != null) {
            reason.setDate(dateFormat.format(date));
            int id = ReasonBll.insertReason(reason);
            if (id > 0) {
                response.setCode(Response.CODE_SUCCESS);
                response.setStatus(Response.SUCCESS_STATUS);
                MissedHoursBll.insertMissedHours(new MissedHours(reason.getEmail(), reason.getHours()));
                Employee employee = EmployeeBll.getEmployee(reason.getEmail());
                Thread thread = new Thread(() -> MailService.getInstance().sendMail("akanyabaymuhammedov@gmail.com", "Reason", "Employee: " + employee.getName() + " can't come to work today. Reason: " + reason.getMotive()));
                thread.start();
                return response;
            }
        }
        response.setCode(Response.AUTH_FAILURE);
        response.setStatus(Response.ERROR_STATUS);
        return response;
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public BaseResponse put(@RequestParam(value = "id") int id, @RequestBody Reason reason) {
        BaseResponse response = new BaseResponse();
        Reason reason1 = ReasonBll.getReason(id);
        if (reason1 == null || reason == null) {
            response.setStatus(Response.ERROR_STATUS);
            response.setCode(Response.AUTH_FAILURE);
            return response;
        }
        reason.setId(id);
        ReasonBll.updateReason(reason);
        response.setStatus(Response.SUCCESS_STATUS);
        response.setCode(Response.CODE_SUCCESS);
        return response;
    }


    @RequestMapping(value = "", method = RequestMethod.DELETE)
    public BaseResponse delete(@RequestParam(value = "id") int id) {
        BaseResponse response = new BaseResponse();
        int result = ReasonBll.deleteReason(id);
        if (result < 0) {
            response.setStatus(Response.ERROR_STATUS);
            response.setCode(Response.AUTH_FAILURE);
            return response;
        }
        response.setStatus(Response.SUCCESS_STATUS);
        response.setCode(Response.CODE_SUCCESS);
        return response;
    }
}
