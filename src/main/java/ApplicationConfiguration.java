import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by akysh_000 on 12/8/2018.
 */

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "controllers")
public class ApplicationConfiguration {
}
