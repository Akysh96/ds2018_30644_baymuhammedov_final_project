package entity;

/**
 * Created by akysh_000 on 1/12/2019.
 */
public class MissedHours {
    private int id, hours;
    private String email;

    public MissedHours(String email, int hours) {
        this.hours = hours;
        this.email = email;
    }

    public MissedHours() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
