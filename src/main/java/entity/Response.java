package entity;

/**
 * Created by akysh_000 on 12/8/2018.
 */
public class Response {
    public final String sharedKey = "SHARED_KEY";
    public static final String SUCCESS_STATUS = "success";
    public static final String ERROR_STATUS = "error";
    public static final int CODE_SUCCESS = 100;
    public static final int AUTH_FAILURE = 102;
}
