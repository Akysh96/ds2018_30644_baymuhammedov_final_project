package bll;

import dao.EmployeeDao;
import dao.EmployeeDaoImpl;
import entity.Employee;

/**
 * Created by akysh_000 on 12/8/2018.
 */
public class EmployeeBll {
    private static EmployeeDao employeeDao = new EmployeeDaoImpl();

    public static Employee getEmployee(String email){
        return employeeDao.getEmployee(email);
    }

    public static Employee getEmployeeById(int id){
        return employeeDao.getEmployeeById(id);
    }

    public static int insertEmployee(Employee employee){
        return employeeDao.insertEmployee(employee);
    }

    public static void updateEmployee(Employee employee){
        employeeDao.updateEmployee(employee);
    }

    public static int deleteEmployee(int id){
        return employeeDao.deleteEmployee(id);
    }
}
