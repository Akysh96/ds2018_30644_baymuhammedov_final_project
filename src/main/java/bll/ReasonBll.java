package bll;

import dao.ReasonDao;
import dao.ReasonDaoImpl;
import entity.Reason;

import java.util.List;

/**
 * Created by akysh_000 on 12/8/2018.
 */
public class ReasonBll {
    private static ReasonDao reasonDao = new ReasonDaoImpl();

    public static List<Reason> getAllReasons(){
        return reasonDao.getAllReasons();
    }

    public static Reason getReason(int id){
        return reasonDao.getReason(id);
    }

    public static int insertReason(Reason reason){
        return reasonDao.insertReason(reason);
    }

    public static void updateReason(Reason reason){
        reasonDao.updateReason(reason);
    }

    public static int deleteReason(int id){
        return reasonDao.deleteReason(id);
    }
}
