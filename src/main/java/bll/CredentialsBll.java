package bll;

import dao.CredentialsDao;
import dao.CredentialsDaoImpl;
import entity.Credentials;

/**
 * Created by akysh_000 on 12/15/2018.
 */
public class CredentialsBll {

    private static CredentialsDao credentialsDao = new CredentialsDaoImpl();

    public static int readType(Credentials credentials) {
        return credentialsDao.readType(credentials);
    }
}
