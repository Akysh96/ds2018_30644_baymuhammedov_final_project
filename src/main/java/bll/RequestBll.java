package bll;

import dao.RequestDao;
import dao.RequestDaoImpl;
import entity.Request;

import java.util.List;

/**
 * Created by akysh_000 on 12/8/2018.
 */
public class RequestBll {
    private static RequestDao requestDao = new RequestDaoImpl();

    public static List<Request> getAllRequests(){
        return requestDao.getAllRequest();
    }

    public static Request getRequest(int id) {
        return requestDao.getRequest(id);
    }

    public static int insertRequest(Request request) {
        return requestDao.insertRequest(request);
    }

    public static void updateRequest(Request request) {
        requestDao.updateRequest(request);
    }

    public static int deleteRequest(int id) {
        return requestDao.deleteRequest(id);
    }
}