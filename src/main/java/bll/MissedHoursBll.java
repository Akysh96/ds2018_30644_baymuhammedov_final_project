package bll;

import dao.MissedHoursDao;
import dao.MissedHoursDaoImpl;
import entity.MissedHours;

/**
 * Created by akysh_000 on 1/12/2019.
 */
public class MissedHoursBll {
    private static MissedHoursDao missedHoursDao = new MissedHoursDaoImpl();

    public static int getMissedHours(String email) {
        return missedHoursDao.getMissedHours(email);
    }

    public static void insertMissedHours(MissedHours missedHours) {
        missedHoursDao.insertMissedHours(missedHours);
    }
}
