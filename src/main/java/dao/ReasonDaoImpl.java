package dao;

import entity.Reason;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import orm.ORMConnection;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;

/**
 * Created by akysh_000 on 12/8/2018.
 */
public class ReasonDaoImpl implements ReasonDao {

    @Override
    public List<Reason> getAllReasons() {
        Session session = ORMConnection.getConnection();
        Transaction transaction = null;
        List<Reason> reasons = new ArrayList<>();
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("from Reason");
            if (query.list().size() > 0) {
                query.list().stream().forEach(e -> {
                    Reason reason = (Reason) e;
                    reasons.add(reason);
                });
            }
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            LOGGER.log(Level.WARNING, "ORM:readAllReason " + e.getMessage());
            e.printStackTrace();
        } finally {
            session.close();
        }
        return reasons;
    }

    @Override
    public Reason getReason(int id) {
        Session session = ORMConnection.getConnection();
        Transaction transaction = null;
        Reason reason = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("from Reason where id = :id");
            query.setParameter("id", id);
            if (query.list().size() > 0) {
                reason = (Reason) query.list().get(0);
            }
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            LOGGER.log(Level.WARNING, "ORM:readReason " + e.getMessage());
            e.printStackTrace();
        } finally {
            session.close();
        }
        return reason;
    }

    @Override
    public int insertReason(Reason reason) {
        Session session = ORMConnection.getConnection();
        Transaction transaction = null;
        int id = 0;
        try {
            transaction = session.beginTransaction();
            id = (int) session.save(reason);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            LOGGER.log(Level.WARNING, "ORM:insertReason " + e.getMessage());
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return id;
    }

    @Override
    public void updateReason(Reason reason) {
        Session session = ORMConnection.getConnection();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(reason);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            LOGGER.log(Level.WARNING, "ORM:updateReason " + e.getMessage());
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public int deleteReason(int id) {
        Session session = ORMConnection.getConnection();
        Transaction tx = null;
        int result = -1;
        try {
            tx = session.beginTransaction();
            result = session.createQuery("Delete from Reason where id =:id").setParameter("id", id).executeUpdate();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            LOGGER.log(Level.WARNING, "ORM:deleteReason " + e.getMessage());
            e.printStackTrace();
        } finally {
            session.close();
        }
        return result;
    }
}
