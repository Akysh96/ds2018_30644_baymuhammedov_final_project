package dao;

import entity.Credentials;

/**
 * Created by akysh_000 on 12/15/2018.
 */
public interface CredentialsDao {

    public int readType(Credentials credentials);
}
