package dao;

import entity.Credentials;
import entity.Employee;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import orm.ORMConnection;

import java.util.logging.Level;

import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;

/**
 * Created by akysh_000 on 12/15/2018.
 */
public class CredentialsDaoImpl implements CredentialsDao {

    @Override
    public int readType(Credentials credentials) {
        Session session = ORMConnection.getConnection();
        Transaction transaction = null;
        int type = -1;
        Credentials credentials1 = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("from Credentials where email = :email and password = :password");
            query.setParameter("email", credentials.getEmail());
            query.setParameter("password", credentials.getPassword());
            if (query.list().size() > 0) {
                credentials1 = (Credentials) query.list().get(0);
                type = credentials1.getType();
            }
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            LOGGER.log(Level.WARNING, "ORM:readType " + e.getMessage());
            e.printStackTrace();
        } finally {
            session.close();
        }
        return type;
    }
}
