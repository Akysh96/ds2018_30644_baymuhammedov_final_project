package dao;

import entity.Request;

import java.util.List;

/**
 * Created by akysh_000 on 12/8/2018.
 */
public interface RequestDao {

    public List<Request> getAllRequest();

    public Request getRequest(int id);

    public int insertRequest(Request request);

    public void updateRequest(Request request);

    public int deleteRequest(int id);
}
