package dao;

import entity.Request;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import orm.ORMConnection;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;

/**
 * Created by akysh_000 on 12/8/2018.
 */
public class RequestDaoImpl implements RequestDao {

    @Override
    public List<Request> getAllRequest() {
        Session session = ORMConnection.getConnection();
        Transaction transaction = null;
        List<Request> requests = new ArrayList<>();
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("from Request");
            if (query.list().size() > 0) {
                query.list().forEach(e -> {
                    Request request = (Request) e;
                    requests.add(request);
                });
            }
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            LOGGER.log(Level.WARNING, "ORM:readRequest " + e.getMessage());
            e.printStackTrace();
        } finally {
            session.close();
        }
        return requests;
    }

    @Override
    public Request getRequest(int id) {
        Session session = ORMConnection.getConnection();
        Transaction transaction = null;
        Request request = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("from Request where id = :id");
            query.setParameter("id", id);
            if (query.list().size() > 0) {
                request = (Request) query.list().get(0);
            }
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            LOGGER.log(Level.WARNING, "ORM:readRequest " + e.getMessage());
            e.printStackTrace();
        } finally {
            session.close();
        }
        return request;
    }

    @Override
    public int insertRequest(Request request) {
        Session session = ORMConnection.getConnection();
        Transaction transaction = null;
        int id = 0;
        try {
            transaction = session.beginTransaction();
            id = (int) session.save(request);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            LOGGER.log(Level.WARNING, "ORM:insertRequest " + e.getMessage());
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return id;
    }

    @Override
    public void updateRequest(Request request) {
        Session session = ORMConnection.getConnection();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(request);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            LOGGER.log(Level.WARNING, "ORM:updateRequest " + e.getMessage());
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public int deleteRequest(int id) {
        Session session = ORMConnection.getConnection();
        Transaction tx = null;
        int result = -1;
        try {
            tx = session.beginTransaction();
            result = session.createQuery("Delete from Request where id =:id").setParameter("id", id).executeUpdate();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            LOGGER.log(Level.WARNING, "ORM:deleteRequest " + e.getMessage());
            e.printStackTrace();
        } finally {
            session.close();
        }
        return result;
    }
}
