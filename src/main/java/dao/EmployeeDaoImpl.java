package dao;

import entity.Employee;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import orm.ORMConnection;

import java.util.logging.Level;

import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;


/**
 * Created by akysh_000 on 12/8/2018.
 */
public class EmployeeDaoImpl implements EmployeeDao {

    @Override
    public Employee getEmployee(String email) {
        Session session = ORMConnection.getConnection();
        Transaction transaction = null;
        Employee employee = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("from Employee where email = :email");
            query.setParameter("email", email);
            if (query.list().size() > 0) {
                employee = (Employee) query.list().get(0);
            }
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            LOGGER.log(Level.WARNING, "ORM:readEmployee " + e.getMessage());
            e.printStackTrace();
        } finally {
            session.close();
        }
        return employee;
    }

    @Override
    public Employee getEmployeeById(int id) {
        Session session = ORMConnection.getConnection();
        Transaction transaction = null;
        Employee employee = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("from Employee where id = :id");
            query.setParameter("id", id);
            if (query.list().size() > 0) {
                employee = (Employee) query.list().get(0);
            }
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            LOGGER.log(Level.WARNING, "ORM:readEmployee " + e.getMessage());
            e.printStackTrace();
        } finally {
            session.close();
        }
        return employee;
    }

    @Override
    public int insertEmployee(Employee employee) {
        Session session = ORMConnection.getConnection();
        Transaction transaction = null;
        int id = 0;
        try {
            transaction = session.beginTransaction();
            id = (int) session.save(employee);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            LOGGER.log(Level.WARNING, "ORM:insertEmployee " + e.getMessage());
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return id;
    }

    @Override
    public void updateEmployee(Employee employee) {
        Session session = ORMConnection.getConnection();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(employee);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            LOGGER.log(Level.WARNING, "ORM:updateEmployee " + e.getMessage());
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public int deleteEmployee(int id) {
        Session session = ORMConnection.getConnection();
        Transaction tx = null;
        int result = -1;
        try {
            tx = session.beginTransaction();
            result = session.createQuery("Delete from Employee where id =:id").setParameter("id", id).executeUpdate();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            LOGGER.log(Level.WARNING, "ORM:deleteEmployee " + e.getMessage());
            e.printStackTrace();
        } finally {
            session.close();
        }
        return result;
    }


}
