package dao;

import entity.MissedHours;

/**
 * Created by akysh_000 on 1/12/2019.
 */
public interface MissedHoursDao {

    public int getMissedHours(String email);

    public void insertMissedHours(MissedHours missedHours);
}
