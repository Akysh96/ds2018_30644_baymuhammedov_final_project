package dao;

import entity.MissedHours;
import entity.Reason;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import orm.ORMConnection;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;

/**
 * Created by akysh_000 on 1/12/2019.
 */
public class MissedHoursDaoImpl implements MissedHoursDao {
    @Override
    public int getMissedHours(String email) {
        Session session = ORMConnection.getConnection();
        Transaction transaction = null;
        int totalMissedHours = 0;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("from MissedHours where email = :email");
            query.setParameter("email", email);
            if (query.list().size() > 0) {
                for (Object obj : query.list()) {
                    MissedHours missedHours = (MissedHours) obj;
                    totalMissedHours += missedHours.getHours();
                }
            }
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            LOGGER.log(Level.WARNING, "ORM:getMissedHours " + e.getMessage());
            e.printStackTrace();
        } finally {
            session.close();
        }
        return totalMissedHours;
    }

    @Override
    public void insertMissedHours(MissedHours missedHours) {
        Session session = ORMConnection.getConnection();
        Transaction transaction = null;
        int id = 0;
        try {
            transaction = session.beginTransaction();
            session.save(missedHours);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            LOGGER.log(Level.WARNING, "ORM:insertMissedHours " + e.getMessage());
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }
}
