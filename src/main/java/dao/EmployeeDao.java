package dao;

import entity.Employee;

/**
 * Created by akysh_000 on 12/8/2018.
 */
public interface EmployeeDao {

    public Employee getEmployee(String email);

    public Employee getEmployeeById(int id);

    public int insertEmployee(Employee employee);

    public void updateEmployee(Employee employees);

    public int deleteEmployee(int id);
}
