package dao;

import entity.Reason;

import java.util.List;

/**
 * Created by akysh_000 on 12/8/2018.
 */
public interface ReasonDao {

    public List<Reason> getAllReasons();

    public Reason getReason(int id);

    public int insertReason(Reason reason);

    public void updateReason(Reason reason);

    public int deleteReason(int id);
}
