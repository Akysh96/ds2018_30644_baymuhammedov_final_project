/**
 * Created by akysh_000 on 12/16/2018.
 */
var app = angular.module('myApp');
app.controller('RequestController',
    function ($http, $scope, $rootScope, $stateParams, $state, $window) {
        $scope.user = $window.localStorage.getItem("username");
        $scope.formSubmit = function () {
            $http({
                url: 'http://localhost:8080/rest/reason',
                method: "POST",
                data: {'email': $scope.user, 'hours': $scope.hours, 'motive': $scope.motive}
            }).then(function (response) {
                    $state.transitionTo('employee');
                },
                function (response) { // optional
                    alert("error!!");
                    $state.transitionTo('employee');
                });
        }
    });