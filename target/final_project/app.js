/**
 * Created by akysh_000 on 12/16/2018.
 */
//app.js
(function () {
    var app = angular.module('myApp', ['ui.router', 'ui.bootstrap']);

    app.run(function ($rootScope, $location, $state, LoginService) {
        console.clear();
        console.log('running');
        if (!LoginService.isAuthenticated()) {
            $state.transitionTo('login');
        }
    });

    app.config(['$stateProvider', '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {
            $stateProvider
                .state('login', {
                    url: '/login',
                    templateUrl: 'login.html',
                    controller: 'LoginController'
                })
                .state('home', {
                    url: '/home',
                    templateUrl: 'admin.html',
                    controller: 'HomeController'
                })
                .state('employee', {
                    url: '/employee',
                    templateUrl: 'employee.html',
                    controller: 'EmployeeController'
                })
                .state('request', {
                    url: '/request',
                    templateUrl: 'RequestForm.html',
                    controller: 'RequestController'
                });

            $urlRouterProvider.otherwise('/login');
        }]);

})();